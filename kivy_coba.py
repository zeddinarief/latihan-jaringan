'''
import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.button import Button

class HelloKivy(App):
	"""docstring for HelloKivy"""
	def build(self):
		return Label(text="Hello Kivy")

helloKivy = HelloKivy()
helloKivy.run()
		
'''
from kivy.app import App
from kivy.uix.button import Button

class TestApp(App):
    def build(self):
        return Button(text='Hello World')

TestApp().run() 